package com.gitlab.holloypolloy.quicksort.zzbad;

/**
 * Implement QuickSort using Lomuto partitioning scheme, see Wikipedia:
 * https://en.wikipedia.org/wiki/Quicksort#Lomuto_partition_scheme
 * It is a direct rewrite to Java from the pseudo-code present in on the wiki page.
 * 
 * It does <b>not</b> work. Can someone point out where I made mistake or 
 * or fix the wikipedia code?
 * 
 * @author holloy.polloy
 */
public class QuickSortLomuto {
	public void sort(int[] a) {
		quicksort(a, 0, a.length - 1);
	}

	private void quicksort(int[] a, int lo, int hi) {
		if (hi <= lo) return;
		int p = partition(a, lo, hi);
		quicksort(a, lo, p - 1);
		quicksort(a, p + 1, hi);
	}

	/**
	 * Partition given array in a way that:
	 * all elements with index < p are smaller then a[p]
	 * all elements with index > p are bigger then a[p]
	 * where p is the partitioning index
	 * 
	 * @return partitioning index
	 */
	private int partition(int[] a, int lo, int hi) {
		int pivot = a[hi];
		int i = lo - 1;
		for (int j = lo; j < hi - 1; j++) {
			// j is always AHEAD of i; that is j > i  
			if (a[j] < pivot) {
				i++;
				swap(a, i, j);
			}
		}
		//if (a[hi] < a[i + 1]) {
		swap(a, i + 1, hi);
		//}
		return i + 1;
	}

	private void swap(int[] a, int p1, int p2) {
		int temp = a[p1];
		a[p1] = a[p2];
		a[p2] = temp; 
	}
}
