package com.gitlab.holloypolloy.quicksort.zzbad;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

/**
 * Test {@link QuickSortLomuto}.
 * 
 * Implement QuickSort using Lomuto partitioning scheme, see Wikipedia:
 * https://en.wikipedia.org/wiki/Quicksort#Lomuto_partition_scheme
 * It is a direct rewrite to Java from the pseudo-code present in on the wiki page.
 * 
 * It does <b>not</b> work. Can someone point out where I made mistake or 
 * or fix the wikipedia code?
 * 
 * @author holloy.polloy
 */
public class QuickSortLomutoTest {
	QuickSortLomuto qs = new QuickSortLomuto();


	@Test
	public void mainTest() throws Exception {
		int[] a        = new int[] {604, 1, 2, 683, 698,   5,  10, 101, 100, 178, 505, 789};
		int[] expected = new int[] {1,   2, 5,  10, 100, 101, 178, 505, 604, 683, 698, 789};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testOneElement() throws Exception {
		int[] a        = new int[] {604};
		int[] expected = new int[] {604};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testTwoElementSorted() throws Exception {
		int[] a        = new int[] {1, 100};
		int[] expected = new int[] {1, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testTwoElementsUnsorted() throws Exception {
		int[] a        = new int[] {100, 1};
		int[] expected = new int[] {1, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4Element_pivotPointIsTheSmallest() throws Exception {
		int[] a        = new int[] {80, 100, 50, 1};
		int[] expected = new int[] {1, 50, 80, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4Elements_pivotPointIsTheGreatest() throws Exception {
		int[] a        = new int[] {80, 1, 50, 100};
		int[] expected = new int[] {1, 50, 80, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4Elements_pivotPointIsTheMiddleValue() throws Exception {
		int[] a        = new int[] {80, 1, 100, 50};
		int[] expected = new int[] {1, 50, 80, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4ElementsReverseSorted() throws Exception {
		int[] a        = new int[] {200, 100, 50, 1};
		int[] expected = new int[] {1, 50, 100, 200};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4ElementsRandomSorted() throws Exception {
		int[] a        = new int[] {200, 50, 100, 1};
		int[] expected = new int[] {1, 50, 100, 200};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test5ElementsReverseSorted() throws Exception {
		int[] a        = new int[] {1000, 200, 100, 50, 1};
		int[] expected = new int[] {1, 50, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test5ElementsRandomUnsorted1() throws Exception {
		int[] a        = new int[] {50, 100, 200, 1000, 1};
		int[] expected = new int[] {1, 50, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test5ElementsRandomUnsorted2() throws Exception {
		int[] a        = new int[] {100, 50, 200, 1, 1000};
		int[] expected = new int[] {1, 50, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test2SameElements() throws Exception {
		int[] a        = new int[] {1, 1};
		int[] expected = new int[] {1, 1};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test3SameElements() throws Exception {
		int[] a        = new int[] {1, 1, 1};
		int[] expected = new int[] {1, 1, 1};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testElements_withReptetition() throws Exception {
		int[] a        = new int[] {100, 50, 100, 200, 50, 1, 1000};
		int[] expected = new int[] {1, 50, 50, 100, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test6Elements_withReptetition_presorted() throws Exception {
		int[] a        = new int[] {1, 1, 50, 50, 100, 100};
		int[] expected = new int[] {1, 1, 50, 50, 100, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test6Elements_withReptetition_unsorted() throws Exception {
		int[] a        = new int[] {1, 50, 1, 100, 50, 100};
		int[] expected = new int[] {1, 1, 50, 50, 100, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4Elements_withReptetition() throws Exception {
		int[] a        = new int[]  {1000, 50, 50, 1000};
		int[] expected = new int[]  {50, 50, 1000, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}
}
