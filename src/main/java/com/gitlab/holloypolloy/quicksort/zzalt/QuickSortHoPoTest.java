package com.gitlab.holloypolloy.quicksort.zzalt;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

/**
 * Test {@link QuickSortHoPo}, my earlier independent attempt 
 * 
 * @author holloy.polloy
 */
@SuppressWarnings("static-access")
public class QuickSortHoPoTest {
	QuickSortHoPo qs = new QuickSortHoPo();


	@Test
	public void mainTest() throws Exception {
		int[] a        = new int[] {604, 1, 2, 683, 698,   5,  10, 101, 100, 178, 505, 789};
		int[] expected = new int[] {1,   2, 5,  10, 100, 101, 178, 505, 604, 683, 698, 789};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testOneElement() throws Exception {
		int[] a        = new int[] {604};
		int[] expected = new int[] {604};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testTwoElementSorted() throws Exception {
		int[] a        = new int[] {1, 100};
		int[] expected = new int[] {1, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testTwoElementsUnsorted() throws Exception {
		int[] a        = new int[] {100, 1};
		int[] expected = new int[] {1, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testThreeElementsUnsorted() throws Exception {
		int[] a        = new int[] {100, 50, 1};
		int[] expected = new int[] {1, 50, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4ElementsReverseSorted() throws Exception {
		int[] a        = new int[] {200, 100, 50, 1};
		int[] expected = new int[] {1, 50, 100, 200};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4ElementsRandomSorted() throws Exception {
		int[] a        = new int[] {200, 50, 100, 1};
		int[] expected = new int[] {1, 50, 100, 200};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test5ElementsReverseSorted() throws Exception {
		int[] a        = new int[] {1000, 200, 100, 50, 1};
		int[] expected = new int[] {1, 50, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test5ElementsRandomUnsorted1() throws Exception {
		int[] a        = new int[] {50, 100, 200, 1000, 1};
		int[] expected = new int[] {1, 50, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test5ElementsRandomUnsorted2() throws Exception {
		int[] a        = new int[] {100, 50, 200, 1, 1000};
		int[] expected = new int[] {1, 50, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test2SameElements() throws Exception {
		int[] a        = new int[] {1, 1};
		int[] expected = new int[] {1, 1};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test3SameElements() throws Exception {
		int[] a        = new int[] {1, 1, 1};
		int[] expected = new int[] {1, 1, 1};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void testElements_withReptetition() throws Exception {
		int[] a        = new int[] {100, 50, 100, 200, 50, 1, 1000};
		int[] expected = new int[] {1, 50, 50, 100, 100, 200, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test6Elements_withReptetition_presorted() throws Exception {
		int[] a        = new int[] {1, 1, 50, 50, 100, 100};
		int[] expected = new int[] {1, 1, 50, 50, 100, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test6Elements_withReptetition_unsorted() throws Exception {
		int[] a        = new int[] {1, 50, 1, 100, 50, 100};
		int[] expected = new int[] {1, 1, 50, 50, 100, 100};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}

	@Test
	public void test4Elements_withReptetition() throws Exception {
		int[] a        = new int[]  {1000, 50, 50, 1000};
		int[] expected = new int[]  {50, 50, 1000, 1000};

		qs.sort(a);

		assertEquals(Arrays.toString(expected), Arrays.toString(a));
	}
}
