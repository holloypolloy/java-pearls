package com.gitlab.holloypolloy.quicksort.zzalt;

/**
 * My earlier independent attempt
 * 
 * @author holloy.polloy
 */
public class QuickSortHoPo {
	public static void sort(int[] a) {
		quicksort(a, 0, a.length - 1);
	}

	private static void quicksort(int[] a, int lo, int hi) {
		if (hi - lo == 0) return;
		if (hi - lo == 1) {
			if (a[lo] > a[hi]) swap(a, lo, hi);
			return;
		}

		int pivot = a[(hi - lo)/2 + lo];
		int i = lo;
		int j = hi;
		while (i < j - 1) {
			while (a[i] < pivot) {
				i++;
			}
			while (a[j] > pivot) {
				j--;
			}
			swap(a, i, j);

			if (a[i] == pivot && a[j] == pivot && i < j - 1) {
				i++;
				if (a[i] > pivot) {
					swap(a, i, j);
				}
			}
		}

		// by this point it must be valid:
		// all elements with index < i are smaller then pivot
		// all elements with index > j are bigger then pivot

		if (lo < i)
			quicksort(a, lo,  i);
		if (j < hi)
			quicksort(a, j, hi);
	}

	private static void swap(int[] a, int p1, int p2) {
		int temp = a[p1];
		a[p1] = a[p2];
		a[p2] = temp; 
	}
}
