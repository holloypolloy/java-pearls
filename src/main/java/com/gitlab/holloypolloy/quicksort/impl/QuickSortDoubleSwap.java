package com.gitlab.holloypolloy.quicksort.impl;

/**
 * Implement QuickSort using partitioning scheme graphically explained in Wikipedia:
 * https://en.wikipedia.org/wiki/Quicksort
 * <br>
 * Main points:<br>
 * (1) pivot is selected as the right most element (like in Lomuto scheme)<br>
 * (2) double swap is used per swapping event; <br>
 * (2a) one to move pivot one element to the left,<br>
 * (2b) the other to swap element tested as bigger with pivot, to go behind it; 
 *     with outed element from previous swap. This element will be tested in
 *     next round.<br>
 * (3) this way left or right side index pointers moves towards each other, 
 *     one of them one step per round.<br>
 * 
 * @author holloy.polloy
 */
public class QuickSortDoubleSwap {
	public void sort(int[] a) {
		quicksort(a, 0, a.length - 1);
	}

	private void quicksort(int[] a, int lo, int hi) {
		if (hi <= lo) return;
		int p = partition(a, lo, hi);
		quicksort(a, lo, p - 1);
		quicksort(a, p + 1, hi);
	}

	/**
	 * Partition given array in a way that:
	 * all elements with index < p are smaller then a[p]
	 * all elements with index > p are bigger then a[p]
	 * where p is the partitioning index
	 * 
	 * @return partitioning index
	 */
	private int partition(int[] a, int lo, int hi) {
		int p = hi; // pivot index
		int i = lo;
		while (i < p) {
			if (a[i] < a[p]) {
				i++;
			} else {
				swap(a, p, p - 1); // move pivot one position to the left using swapping 
				if (p - i > 1) {
					swap(a, i, p);  
					// swap current element, tested as greater then pivot, with the outed 
					// element from the previous swap, that is just behind pivot.
					// Don't do this swap in the last round of this loop, when i and p lays 
					// next to each, because it would just negate previous swap.
				}
				p = p - 1;
			}
		}
		return p;
	}

	private void swap(int[] a, int p1, int p2) {
		int temp = a[p1];
		a[p1] = a[p2];
		a[p2] = temp; 
	}
}
