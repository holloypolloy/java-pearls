package com.gitlab.holloypolloy.quicksort.impl;

/**
 * Implement QuickSort using Hoare's partitioning scheme, see Wikipedia:
 * See: https://en.wikipedia.org/wiki/Quicksort#Hoare_partition_scheme
 * 
 * @author holloy.polloy
 */
public class QuickSortHoare {
	public void sort(int[] a) {
		quicksort(a, 0, a.length - 1);
	}

	private void quicksort(int[] a, int lo, int hi) {
		if (hi - lo == 0) return;
		int p = partition(a, lo, hi);
		quicksort(a, lo, p);
		quicksort(a, p + 1, hi);
	}

	/**
	 * Partition given array in a way that:
	 * all elements with index < p are smaller then a[p]
	 * all elements with index > p are bigger then a[p]
	 * where p is the partitioning index
	 * @return partitioning index
	 */
	private int partition(int[] a, int lo, int hi) {
		int pivot = a[(hi - lo)/2 + lo];
		int i = lo - 1;
		int j = hi + 1;
		while (true) {
			while (a[++i] < pivot);
			while (a[--j] > pivot);
			if (i >= j) {
				return j;
			}
			swap(a, i, j);
		}
	}

	private void swap(int[] a, int p1, int p2) {
		int temp = a[p1];
		a[p1] = a[p2];
		a[p2] = temp; 
	}
}
