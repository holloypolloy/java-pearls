package com.gitlab.holloypolloy.permutator;

import static com.gitlab.holloypolloy.factorial.Factorial.factorial;
import static com.gitlab.holloypolloy.permutator.TestUtils.loopCombinationsAssertItsUnique;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import com.gitlab.holloypolloy.permutator.impl.IterativePermutator;

/**
 * Test {@link IterativePermutator}.
 * 
 * @author holloy.polloy
 */
public class IterativePermutatorTest {

	// simple printout
	public static void main(String[] args) {
		List<int[]> combinations = IterativePermutator.permute(new int[] { 1, 2, 3, 4 });
		for (int[] combination : combinations) {
			System.out.println(Arrays.toString(combination));
		}
	}

	@Test
	public void everyCombinationHasToBeUnique_3elements() {
		int[] input = new int[] { 1, 2, 3 };
		List<int[]> combinations = IterativePermutator.permute(input);
		loopCombinationsAssertItsUnique(combinations.iterator(), input, factorial(3));
	}

	@Test
	public void everyCombinationHasToBeUnique_4elements() {
		int[] input = new int[] { 1, 2, 3, 4 };
		List<int[]> combinations = IterativePermutator.permute(input);
		loopCombinationsAssertItsUnique(combinations.iterator(), input, factorial(4));
	}

	@Test
	public void everyCombinationHasToBeUnique_5elements() {
		int[] input = new int[] { 1, 2, 3, 4, 5 };
		List<int[]> combinations = IterativePermutator.permute(input);
		loopCombinationsAssertItsUnique(combinations.iterator(), input, factorial(5));
	}
	
	@Test
	public void everyCombinationHasToBeUnique_unsorted5elements() {
		int[] input = new int[] { 70, 66, 52, 43, 39 };
		List<int[]> combinations = IterativePermutator.permute(input);
		loopCombinationsAssertItsUnique(combinations.iterator(), input, factorial(5));
	}
}
