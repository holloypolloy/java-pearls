package com.gitlab.holloypolloy.permutator.impl;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Heap's algorithm (by B. R. Heap, published 1963) implemented as iterator.
 * https://en.wikipedia.org/wiki/Heap%27s_algorithm
 * 
 * @author holloy.polloy
 */
public class HeapsPermutatorIterator implements Iterator<int[]> {
	private int[] a;
	private int n;
	private int[]c;
	private int i;
	private boolean finished;
	private int[] delayedResult;

	public HeapsPermutatorIterator(int[] a) {
		this.a = a;
		n = a.length;
		c = createArrayFilledWithZeroes(n);
		i = 0;
		finished = false;
		delayedResult = result(a);
	}

	@Override
	public boolean hasNext() {
		return !finished;
	}
	
	@Override
	public int[] next() {
		finished = true;
		int[] r = delayedResult;

		while (i < n) {
			if (c[i] < i) {
				if (isEven(i)) {
					swap(a, 0, i);
				} else {
					swap(a, c[i], i);
				}
				c[i] = c[i] + 1;
				i = 0;
				delayedResult = result(a);
				finished = false;
				break;
			} else {
				c[i] = 0;
				i++;
			}
		}
		
		return r;
	}

	private boolean isEven(int number) {
		return number % 2 == 0;
	}

	/**
	 * Swap elements in given array using two provided indexes to that array.
	 */
	private void swap(int[] a, int p1, int p2) {
		if (p1 < a.length && p2 < a.length) {
			int x = a[p1];
			a[p1] = a[p2];
			a[p2] = x;
		}
	}
	
	/**
	 * Initialise new int[] array and set all elements to 0
	 * 
	 * @param n size of the new array
	 */
	private int[] createArrayFilledWithZeroes(int n) {
		int[] r = new int[n];
		for (int i = 0; i < n; i++) {
			r[i] = 0;
		}
		return r;
	}

	/**
	 * Create a defensive array copy of the result so by publishing it out the
	 * internal "in permutation" array cannot be disturbed.
	 */
	protected int[] result(int[] a) {
		return Arrays.copyOf(a, n);
	}
}
