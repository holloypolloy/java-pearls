package com.gitlab.holloypolloy.permutator.impl;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * My own iterative, non-recursive reworking of
 * {@link RecursivePermutator}.
 * 
 * Benefit of iterative solution is that it can be reworked as {@link Iterator}
 * and provide results in memory efficient, get next when requested, discard
 * after, fashion.
 * 
 * @author holloy.polloy
 */
public class IterativePermutator {

	public static List<int[]> permute(int[] a) {
		List<int[]> result = new LinkedList<>(); // assumption is that result would be accessed sequentially 

		// J stack stores "vertical" positions for every depth; 
		// it's the for loop counter equivalent from recursive solution  
		int[] stackJ = new int[a.length];
		// H stack stores 0 or 1, where 0 means fist half of the loop from recursive solution, 1 the second half 
		// that is if we are entering node or leaving node in tree traversal terminology
		int[] stackH = new int[a.length];
		
		// Fill J stack with start values 
		// for n == 4 stack would have initial values of [0, 1, 2, 3]
		// Stack is "reset" to this values as part of clean behind when "going up" (moving back "left" when branch is "exhausted"). 
		for (int i = 0; i < stackJ.length; i++) {
			stackJ[i] = i; 
		}
		// H stack initial value is 0s everywhere (first halfs)
		for (int i = 0; i < stackH.length; i++) {
			stackH[i] = 0; 
		}
		int p = 0; // depth pointer (depth index)

		while (p >= 0) {

			if (p == a.length) {
				result.add(Arrays.copyOf(a, a.length));
				p--;
			} else {
				if (stackH[p] == 0) {
					// first half of the for loop
					swap(a, p, stackJ[p]); // swap
					stackH[p] = 1;
					p++;
				} else {
					// second half of the for loop
					stackH[p] = 0;
					swap(a, stackJ[p], p); // un-swap
					stackJ[p] = stackJ[p] + 1;
					if (stackJ[p] == a.length) {
						stackJ[p] = p; // reset stack behind (prepare it for next "dive")
						p--;
					}
				}
			}
		}
		return result;
	}

	private static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}