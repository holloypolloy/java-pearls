package com.gitlab.holloypolloy.permutator.impl;

import java.util.Arrays;
import java.util.List;

/**
 * Heap's algorithm (by B. R. Heap, published 1963)
 * https://en.wikipedia.org/wiki/Heap%27s_algorithm
 */
public class HeapsPermutator {

	public static List<int[]> permute(int[] a) {
		int n = a.length;

		// initialize result
		int[][] r = new int[factorial(n)][n];

		// set helper array C, all elements to 0
		int[] c = new int[n];
		for (int i = 0; i < n; i++) {
			c[i] = 0;
		}

		int rc = 0;
		r[rc++] = Arrays.copyOf(a, n);

		int i = 0;
		while (i < n) {
			if (c[i] < i) {
				if (i % 2 == 0) {
					swap(a, 0, i);
				} else {
					swap(a, c[i], i);
				}
				r[rc++] = Arrays.copyOf(a, n);
				c[i] = c[i] + 1;
				i = 0;
			} else {
				c[i] = 0;
				i++;
			}
		}

		return Arrays.asList(r);
	}

	private static void swap(int[] a, int p1, int p2) {
		if (p1 < a.length && p2 < a.length) {
			int x = a[p1];
			a[p1] = a[p2];
			a[p2] = x;
		}
	}

	private static int factorial(int n) {
		if (n == 0) {
			return 1;
		}
		int r = 1;
		for (int i = 1; i <= n; i++) {
			r *= i;
		}
		return r;
	}
}
