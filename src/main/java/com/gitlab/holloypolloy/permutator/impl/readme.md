# Permutation Algorithms

Get all unique combinations of given array or collection. For simplicity I considered only arrays of integers.
Example:

    [1, 2, 3, 4]
    [1, 2, 4, 3]
    [1, 3, 2, 4]
    [1, 3, 4, 2]
    [1, 4, 3, 2]
    [1, 4, 2, 3]
    [2, 1, 3, 4]
    [2, 1, 4, 3]
    [2, 3, 1, 4]
    ...

## Heap's Permutator 

[HeapsPermutator](HeapsPermutator.java) is implementation of Heap's algorithm (by B. R. Heap, published 1963).
It's algorithm with lowest number of swaps, just one for every output combination. 
Details in [Wikipedia for example](https://en.wikipedia.org/wiki/Heap%27s_algorithm).
It's the most difficult to understand solution.

Also note that the output is somewhat _un-intuitively_ sorted compared to recursive solution.

I also include Heap's solution reworked as iterator, see [HeapsPermutatorIterator](HeapsPermutatorIterator.java). 
    
## Recursive solution

Elegant and simple, based on swaps and un-swaps, returning the permutated array always back to the same state with every loop close. 
This solution do more swaps then Heap's algorithm but swaps in memory are cheap, performance wise they would be comparable.

Code of [RecursivePermutator](RecursivePermutator.java) is a gently improved version of code published here: http://www.programcreek.com/2013/02/leetcode-permutations-java/

Problem with this solution is that it can be used as Iterator; it has to keep whole result in memory.
This can be remedied by using callbacks, where results are immediately processed as they become available, discarded afterwards. 
But using callbacks can be very intrusive to other code in many situations.

## Iterative Permutator

Benefit of iterative (non-recursive) solution is that it can be easily modified to be a standard Iterator.
For larger sets number some kind iterative solution is a must; as number of combinations grows exponentially storing the whole result would soon take whole available memory.  

In my solution I drew heavily from recursive solution. The original recursive formula is _tail non recursive_ so converting it to iterative solution is not trivial.   
There are my references in source code referencing the recursive implementation so it's best to read that one first.
It can also be used as an example how to rework non trivial recursive solution to iterative.

There is an alternative approach how to perceive this solution: the problem can be imagined as Deep First Traversal of a tree graph.
   - branches are virtual, representing combination possibilities
   - by going deep (ie down or right) represents narrowing sub-array to be further permutated (right side; left side stays constant for the branch) 
   - only terminal states are added to results
   - permutated array is swaped when _before_ visiting node and un-swapped when _after_ visiting node (leaving node).
   - first round does swaps with itself in every step when going down, this way we get first result identical to input, that what is preferred
   

Branch visualisation:

    A1 [1, 2, 3, 4] p = 0
      B2 [1, 2, 3, 4] p = 1
	    C1 [1, 2, 3, 4] p = 2
	      D1 [1, 2, 3, 4] p = 3 => add to results
	    C2 [1, 2, 4, 3]
	      D1 [1, 2, 4, 3] p = 3 => add to results
	  B3 [1, 3, 2, 4] p = 1
	    . . .
      B4 [1, 4, 3, 2] p = 1
	    . . .
    
    A2 [2, 1, 3, 4] p = 0
      B2 [2, 1, 3, 4] p = 1
        . . .
      B3 [2, 3, 1, 4] p = 1
        . . .
	  B4 [2, 4, 2, 1] p = 1
	    . . .
    
    A3 [3, 2, 1, 4] p = 0
      B2 . . .
      B3 . . .
	  B4 . . .
   
    A4 [4, 2, 3, 1]
      B2 . . .
      B3 . . .
	  B4 . . .
	  
I also include Heap's solution reworked as iterator, see [IterativePermutatorIterator](IterativePermutatorIterator.java). 