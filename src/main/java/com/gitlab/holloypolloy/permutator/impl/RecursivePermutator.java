package com.gitlab.holloypolloy.permutator.impl;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Gently improved code of recursive algorithm from http://www.programcreek.com/2013/02/leetcode-permutations-java/<br>
 * Get rid of auto-boxing integers of the result, more memory efficient; also with less code.
 */
public class RecursivePermutator {

	public static List<int[]> permute(int[] a) {
		List<int[]> result = new LinkedList<>(); // assumption is that result would be accessed sequentially 
		permute(a, 0, result);
		return result;
	}

	private static void permute(int[] a, int start, List<int[]> result) {
		if (start >= a.length) {
			// when bottom of recursion is reached
			// add (copy of) current state to results and quit
			result.add(Arrays.copyOf(a, a.length));
		} else {
			for (int j = start; j < a.length; j++) {
				swap(a, start, j);
				permute(a, start + 1, result);
				swap(a, start, j); // undo change; 
				// at the end of each loop array is always in the same state 
				// as was on entry to this method
			}
		}
	}

	private static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}
