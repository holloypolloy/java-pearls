package com.gitlab.holloypolloy.permutator.impl;

import java.util.Arrays;
import java.util.Iterator;

import com.gitlab.holloypolloy.permutator.impl.IterativePermutator;

/**
 * Variant of {@link IterativePermutator} implemented as {@link Iterator}.
 * 
 * Iterator approach is a must when dealing with larger inputs, as memory to
 * hold all combinations can easily exceed available memory.
 *
 * @author holloy.polloy
 */
public class IterativePermutatorIterator implements Iterator<int[]> {
	private final int[] a; // interim state and current combination holder between iterations
	private int p; // depth pointer (depth index)

	// J stack stores "vertical" positions for every depth; 
	// it's the for loop counter equivalent from recursive solution  
	private final int[] stackJ;
	// H stack stores 0 or 1, where 0 means fist half of the loop from recursive solution, 1 the second half 
	// that is if we are entering node or leaving node in tree traversal terminology
	private final int[] stackH;
	
	private boolean first = true;
	private int[] nextResult;
	                       
	public IterativePermutatorIterator(int[] input) {
		a = input;
		
		stackJ = new int[a.length];
		stackH = new int[a.length];
		
		// Fill J stack with start values 
		// for n == 4 stack would have initial values of [0, 1, 2, 3]
		// Stack is "reset" to this values as part of clean behind when "going up" (moving back "left" when branch is "exhausted"). 
		for (int i = 0; i < stackJ.length; i++) {
			stackJ[i] = i; 
		}
		// H stack initial value is 0s everywhere (first halfs)
		for (int i = 0; i < stackH.length; i++) {
			stackH[i] = 0; 
		}
		p = 0; // depth pointer (depth index)
	}

	@Override
	public boolean hasNext() {
		if (first) {
			nextResult = next0();
			first = false;
		}
		return (nextResult != null);
	}

	@Override
	public int[] next() {
		int[] currentResult = nextResult;
		nextResult = next0();
		return currentResult;
	}
	
	public int[] next0() {
		while (p >= 0) {

			if (p == a.length) {
				p--;
				return Arrays.copyOf(a, a.length);
			} else {
				if (stackH[p] == 0) {
					// first half of the for loop
					swap(a, p, stackJ[p]); // swap
					stackH[p] = 1;
					p++;
				} else {
					// second half of the for loop
					stackH[p] = 0;
					swap(a, stackJ[p], p); // un-swap
					stackJ[p] = stackJ[p] + 1;
					if (stackJ[p] == a.length) {
						stackJ[p] = p; // reset stack behind (prepare it for next "dive")
						p--;
					}
				}
			}
		}
		return null;
	}

	private void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
}
