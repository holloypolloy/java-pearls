package com.gitlab.holloypolloy.permutator;

import static com.gitlab.holloypolloy.factorial.Factorial.factorial;
import static com.gitlab.holloypolloy.permutator.TestUtils.loopCombinationsAssertItsUnique;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import com.gitlab.holloypolloy.permutator.impl.IterativePermutatorIterator;

/**
 * Test {@link IterativePermutatorIterator}.
 * 
 * Return permutator implemented as Iterator is more memory efficient as only
 * one combination can be kept in memory and discarded as they are processed.
 * Very useful in situations where caller may not consume all combinations.
 * 
 * @author holloy.polloy
 */
public class IterativePermutatorIteratorTest {

	// basic printout visual test
	public static void main(String[] args) {
		IterativePermutatorIterator combinations = new IterativePermutatorIterator(new int[] { 1, 2, 3, 4 });
		while (combinations.hasNext()) {
			System.out.println(Arrays.toString(combinations.next()));
		}
	}

	@Test
	public void everyCombinationHasToBeUnique_3elements() {
		int[] input = new int[] { 1, 2, 3 };
		IterativePermutatorIterator permutator = new IterativePermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(3));
	}

	@Test
	public void everyCombinationHasToBeUnique_4elements() {
		int[] input = new int[] { 1, 2, 3, 4 };
		IterativePermutatorIterator permutator = new IterativePermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(4));
	}

	@Test
	public void everyCombinationHasToBeUnique_5elements() {
		int[] input = new int[] { 1, 2, 3, 4, 5 };
		IterativePermutatorIterator permutator = new IterativePermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(5));
	}
	
	@Test
	public void everyCombinationHasToBeUnique_unsorted5elements() {
		int[] input = new int[] { 70, 66, 52, 43, 39 };
		IterativePermutatorIterator permutator = new IterativePermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(5));
	}
	
	@Test
	public void only1ElementInput() {
		int[] input = new int[] { 100 };
		IterativePermutatorIterator permutator = new IterativePermutatorIterator(input);
		assertTrue(permutator.hasNext());
		assertEquals("[100]", Arrays.toString(permutator.next()));
		assertFalse(permutator.hasNext());
	}
	
	@Test
	public void emptyInput() {
		int[] input = new int[] { };
		IterativePermutatorIterator permutator = new IterativePermutatorIterator(input);
		assertTrue(permutator.hasNext());
		assertEquals("[]", Arrays.toString(permutator.next()));
		assertFalse(permutator.hasNext());
	}
}
