package com.gitlab.holloypolloy.permutator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestUtils {
	/**
	 * Loop through all combination of the given input using given
	 * implementation; for every combination assert it's unique
	 * 
	 * @param permutator
	 *            permutator implementation
	 * @param input
	 *            input array to be permutated; as number grows exponentially
	 *            use only small inputs
	 * @param numberOfIterations
	 *            number of expected iterations and at the same time number of
	 *            unique combinations
	 */
	public static void loopCombinationsAssertItsUnique(
			Iterator<int[]> permutator, 
			int[] input,
			long numberOfIterations) {
		Set<String> uniquityCache = new HashSet<>();
		long loopCounter = 0;
		while (permutator.hasNext()) {
			int[] permutation = permutator.next();
			String permutationStr = Arrays.toString(permutation);
			if (uniquityCache.contains(permutationStr)) {
				fail("Combination " + permutationStr + " was created twice!");
			}
			uniquityCache.add(permutationStr);
			loopCounter++;
		}
		assertEquals(numberOfIterations, loopCounter);
		assertEquals(numberOfIterations, uniquityCache.size());
	}
}
