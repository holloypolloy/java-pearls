package com.gitlab.holloypolloy.permutator;

import static com.gitlab.holloypolloy.factorial.Factorial.factorial;
import static com.gitlab.holloypolloy.permutator.TestUtils.loopCombinationsAssertItsUnique;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Test;

import com.gitlab.holloypolloy.permutator.impl.HeapsPermutatorIterator;

/**
 * Test {@link HeapsPermutatorIterator}, array combinations iterator based on
 * Heaps algorithm; the most effective algorithm in terms of number of swaps.
 * 
 * Return permutator implemented as Iterator is more memory efficient as only
 * one combination can be kept in memory and discarded as they are processed.
 * Very useful in situations where caller may not consume all combinations.
 * 
 * @author holloy.polloy
 */
public class HeapsPermutatorIteratorTest {

	// basic printout visual tests
	public static void main(String[] args) {
		HeapsPermutatorIterator combinations = new HeapsPermutatorIterator(new int[] { 1, 2, 3, 4 });
		while (combinations.hasNext()) {
			System.out.println(Arrays.toString(combinations.next()));
		}
	}

	@Test
	public void everyCombinationHasToBeUnique_3elements() {
		int[] input = new int[] { 1, 2, 3 };
		HeapsPermutatorIterator permutator = new HeapsPermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(3));
	}

	@Test
	public void everyCombinationHasToBeUnique_4elements() {
		int[] input = new int[] { 1, 2, 3, 4 };
		HeapsPermutatorIterator permutator = new HeapsPermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(4));
	}

	@Test
	public void everyCombinationHasToBeUnique_5elements() {
		int[] input = new int[] { 1, 2, 3, 4, 5 };
		HeapsPermutatorIterator permutator = new HeapsPermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(5));
	}
	
	@Test
	public void everyCombinationHasToBeUnique_unsorted5elements() {
		int[] input = new int[] { 70, 66, 52, 43, 39 };
		HeapsPermutatorIterator permutator = new HeapsPermutatorIterator(input);
		loopCombinationsAssertItsUnique(permutator, input, factorial(5));
	}
	
	@Test
	public void only1ElementInput() {
		int[] input = new int[] { 100 };
		HeapsPermutatorIterator permutator = new HeapsPermutatorIterator(input);
		assertTrue(permutator.hasNext());
		assertEquals("[100]", Arrays.toString(permutator.next()));
		assertFalse(permutator.hasNext());
	}
	
	@Test
	public void emptyInput() {
		int[] input = new int[] { };
		HeapsPermutatorIterator permutator = new HeapsPermutatorIterator(input);
		assertTrue(permutator.hasNext());
		assertEquals("[]", Arrays.toString(permutator.next()));
		assertFalse(permutator.hasNext());
	}
}
