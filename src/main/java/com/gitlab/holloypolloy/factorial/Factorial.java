package com.gitlab.holloypolloy.factorial;

/**
 * Compute factorial of given positive integer<br/>
 * Example:
 * <pre>
 * 3! = 3 * 2 * 1 = 6
 * 4! = 4 * 3 * 2 * 1 = 24 
 * </pre>
 */
public class Factorial {
	public static long factorial(int n) {
		if (n == 0 || n == 1) {
			return 1;
		}
		int r = 1;
		for (int i = 1; i <= n; i++) {
			r *= i;
		}
		return r;
	}
}
