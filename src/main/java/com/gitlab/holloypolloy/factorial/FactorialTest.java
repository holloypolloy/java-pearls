package com.gitlab.holloypolloy.factorial;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Compute factorial of given integer<br/>
 * Example:
 * <pre>
 * 3! = 3 * 2 * 1 = 6
 * 4! = 4 * 3 * 2 * 1 = 24 
 * </pre>
 */
public class FactorialTest {
	@Test
	public void test() {
		assertEquals(0, Factorial.factorial(0));
		assertEquals(1, Factorial.factorial(1));
		assertEquals(2 * 1, Factorial.factorial(2));
		assertEquals(3 * 2 * 1, Factorial.factorial(3));
		assertEquals(4 * 3 * 2 * 1, Factorial.factorial(4));
		assertEquals(5 * 4 * 3 * 2 * 1, Factorial.factorial(5));
		
		// big number
		assertEquals(87_178_291_200L, Factorial.factorial(14));
	}
}
